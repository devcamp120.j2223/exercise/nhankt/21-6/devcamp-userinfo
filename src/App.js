import {gUserInfo} from "./info"
import image from "./assets/images/image.webp"


function App() {
  return (
    <div >
      <h5>{gUserInfo.firstname + " " +gUserInfo.lastname}</h5>
      <img src={image} width="300px" alt="avata"></img>
      <p>{gUserInfo.age}</p>
      <p>{gUserInfo.age >35 ? "he is old":"he is young"}</p>
      <ul>
        {
          gUserInfo.language.map((element, index) => {
            return <li key={index}>{element}</li>
          })
        }
      </ul>
    </div>
  );
}

export default App;
